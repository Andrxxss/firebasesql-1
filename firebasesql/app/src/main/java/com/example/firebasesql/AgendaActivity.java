package com.example.firebasesql;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AgendaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "bd_usuarios", null, 1);
    }

    public void onClick(View view){
        Intent miIntent = null;
        switch (view.getId()){
            case R.id.btnRegistrar:
                miIntent = new Intent(AgendaActivity.this, RegistroUsuarioActivity.class);
                break;
            case R.id.btnConsultar:
                miIntent = new Intent(AgendaActivity.this, ConsultarUsuariosActivity.class);
                break;
            case R.id.btnConsultarSP:
                miIntent = new Intent(AgendaActivity.this, ConsultaComboActivity.class);
                break;
            case R.id.btnConsultarLV:
                miIntent = new Intent(AgendaActivity.this, ConsultarListaLetsViewActivity.class);
                break;
        }
        if(miIntent != null){
            startActivity(miIntent);
        }
    }

    public void buttonPress(View view){
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }



}