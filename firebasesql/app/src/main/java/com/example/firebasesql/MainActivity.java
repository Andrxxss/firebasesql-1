package com.example.firebasesql;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText rePasswordEditText;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        rePasswordEditText = findViewById(R.id.rePasswordEditText);

        mAuth = FirebaseAuth.getInstance();

    }
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {
        Log.i("User:", ""+currentUser);
    }

    public void createAccount(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("ÉXITO", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            startActivity(new Intent(MainActivity.this, AgendaActivity.class));
                            finish();
                        } else {
                            Log.w("ERROR", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Error => Datos incorrectos.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    public void buttonPress(View view){
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String rePass = rePasswordEditText.getText().toString();

        if(!email.isEmpty() && !password.isEmpty() && !rePass.isEmpty()){
            if(password.equals(rePass)){
                if(password.length()>5){

                    createAccount(email, password);

                }else
                    Toast.makeText(this, "La contraseña debe tener mínimo 6 carácteres", Toast.LENGTH_SHORT).show();

            }else
                Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();

        }else
            Toast.makeText(this, "Por favor completa todos los campos", Toast.LENGTH_SHORT).show();

    }

    public void iniciarSesionButton(View view){
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void playMusic(){
        startService(new Intent(this, MusicActivity.class));
    }
    public void stopMusic(){
        stopService(new Intent(this, MusicActivity.class));
    }

    @Override
    public void onPause() {
        super.onPause();
        {
            playMusic();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        {
            stopMusic();
        }
    }







}