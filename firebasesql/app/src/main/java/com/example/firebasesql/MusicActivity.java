package com.example.firebasesql;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class MusicActivity extends Service {

    MediaPlayer myPlayer;

    public  void onCreate(){
        super.onCreate();
        myPlayer = MediaPlayer.create(this, R.raw.cancion);
        myPlayer.setLooping(true);

    }

    public int onStartCommand(Intent intent, int flags, int startId){
        myPlayer.start();
        return  START_STICKY;
    }

    public void  onDestroy(){
        super.onDestroy();
        if(myPlayer.isPlaying()){
            myPlayer.stop();
            myPlayer.release();
            myPlayer = null;
        }

    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

